<?php
/**
 * @file
 * views field handler for eazy edit
 */
/**
 * Field handler to present a link node edit.
 */
class eazyedit_views_handler_field_links extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['nid'] = array('table' => 'node', 'field' => 'nid');
    $this->additional_fields['type'] = array('table' => 'node', 'field' => 'type');
    $this->additional_fields['status'] = array('table' => 'node', 'field' => 'status');
  }

  function query() {
    $this->add_additional_fields();
  }

  function render($values) {
    $node = new stdClass();
    $node->nid = $values->nid;
    $node->type = $values->node_type;
    $node->status = $values->node_status;
    return eazyedit_node_links($node);
  }
}