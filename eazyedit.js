Drupal.behaviors.eazyedit = function (context) {
	if ($.browser.msie && $.browser.version.substr(0,1)<7); //skip ie6
	else {
		$(context).find('.eazyedit').each(function() {
			var wrapper = null;
			if ($(this).parent().parent().parent().hasClass('views-row')) wrapper = $(this).parent().parent().parent();
			else if ($(this).parent().parent().hasClass('node') 
				|| $(this).parent().parent().hasClass('block') 
				|| $(this).parent().parent().hasClass('comment') 
				|| $(this).parent().parent().hasClass('views-row'))	wrapper = $(this).parent().parent();
			else wrapper = $(this).parent();
			$(wrapper).addClass('eazyedit-wrapper');
			
			$(this).append(' | ');
			$(this).append('<a href="javascript:;" class="ee-close" title="hide this temporarily">x</a>');
			$(this).find('a.ee-close').click(function(){ $(this).parent().css('display', 'none') });
		});
		
		/* The following uses jquery for the hover effect so it could work in IE6 and it creates a delayed hover effect, but it was causing flickering issues when one wrapper was contained in another so we're not using it until we can get that fixed
		$('.eazyedit-wrapper').hover(
			function() { 
				$(this).addClass('eazy-hover'); 
				hover_intent = setTimeout( function(){ $('.eazy-hover').find('.eazyedit').show(); } , 500 ); 
			},
			function() { 
				try{clearTimeout(hover_intent);}
				catch(e){}; 
				$('.eazy-hover').find('.eazyedit').hide(); 
				$(this).removeClass('eazy-hover'); 
			}
		);
		*/
	}
};