<?php
/**
 * @file
 * views include for eazy edit
 */
/**
 * Implementation of hook_views_handlers().
 */
function eazyedit_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'eazyedit'),
    ),
    'handlers' => array(
      'eazyedit_views_handler_field_links' => array(
        'parent' => 'views_handler_field_node_link',
      ),
    ),
  );
}
/**
 * Implementation of hook_views_data().
 */
function eazyedit_views_data() {
  $data['eazyedit']['table']['group']  = t('Node');
  $data['eazyedit']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $data['eazyedit']['links'] = array(
  'field' => array(
      'handler' => 'eazyedit_views_handler_field_links',
      'title' => t('Eazy Edit'),
      'help' => t('Provide eazy edit links for the node.'),
    ),
  );

  return $data;
}
